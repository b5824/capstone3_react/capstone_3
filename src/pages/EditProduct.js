import React from 'react';
import { Button, Form, Modal, Row, Col } from 'react-bootstrap';
import './RegisterUser.css';
import { useState, useEffect, useContext, useParams } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function EditProducts() {
  // const history = useNavigate();
  // const { user } = useContext(UserContext);
  // const {productId} = useParams();

  // const [quantity, setQuantity] = useState("");
  // const [name, setName] = useState("");
  // const [description, setDescription] = useState("");
  // const [categories, setCategories] = useState("");
  // const [price, setPrice] = useState("");

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // console.log(user)

  // function updateProduct(e) {
  //   e.preventDefault();
  //   console.log(e);

  //   fetch(`https://intense-citadel-80119.herokuapp.com/seller/edit/${productId}`, {
  //     method: "PUT",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //     body: JSON.stringify({
  //       name: name,
  //       description: description,
  //       categories: categories,
  //       price: price,
  //       quantity: quantity

  //     }),
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       console.log(data);

  //       if (data) {
  //         alert("Updated!")

  //         history("/")
  //       } else {
  //         alert("Something went wrong")
  //       }
  //     });

  //   setName("");
  //   setDescription("");
  //   setQuantity(0);
  //   setPrice(0);
  //   setCategories("");
  // }

  return (
    <>
      <Button className='m-1' variant='success' onClick={handleShow}>
        Add
      </Button>
      <Modal show={show} onHide={handleClose} className='card_product'>
        <Row xs={12} md={12}>
          <Form className='formmodal__xx p-5'>
            <h2>Update your product</h2>
            {/*onSubmit={(e) => updateProduct(e)}>*/}

            <Form.Group as={Col} className='mb-3' controlId='productName'>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type='text'
                placeholder='Enter updated name'
                required
              />
            </Form.Group>

            <Form.Group as={Col} className='mb-3' controlId='description'>
              <Form.Label>Description</Form.Label>
              <Form.Control
                type='text'
                placeholder='description'
                required
                // value={description}
                // onChange={(e) => setDescription(e.target.value)}
              />
            </Form.Group>

            <Form.Group className='mb-3' controlId='quantity'>
              <Form.Label>Quantity</Form.Label>
              <Form.Control
                type='text'
                placeholder='quantity'
                required
                // value={quantity}
                // onChange={(e) => setQuantity(e.target.value)}
              />
            </Form.Group>
            <Form.Group className='mb-3' controlId='price'>
              <Form.Label>Price</Form.Label>
              <Form.Control
                type='text'
                placeholder='price'
                required
                // value={price}
                // onChange={(e) => setPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId='formGridState'>
              <Form.Label>Category</Form.Label>
              <Form.Select
                className='dropdownMenu'
                // value={categorySelected}
                // onChange={(e) => setCategorySelected(e.target.value)}
                scrollable={true}
              >
                {/* {category}*/}
              </Form.Select>
            </Form.Group>
          </Form>
          <Modal.Footer>
            <Button variant='secondary' onClick={handleClose}>
              Close
            </Button>
            <Button variant='primary' onClick={handleClose} className='mx-3'>
              Save Changes
            </Button>
          </Modal.Footer>
        </Row>
      </Modal>
    </>
  );
}
